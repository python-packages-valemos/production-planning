import unittest
from pathlib import Path

from production_planning.production_graph.operations import build_recipe_result_graph, render
from production_planning.objects.material_rate import MaterialRate
from production_planning.objects.material_rate_set import MaterialRateSet
from production_planning.objects.recipes_collection import RecipesCollection, ProductionRecipe
from production_planning.production_graph.production_node import ProductionNode


class TestTreeBuilder(unittest.TestCase):

    def test_basic_tree_build(self):
        recipes = RecipesCollection()
        
        # mine coal
        recipes.add_recipe(ProductionRecipe(
            ingredients=MaterialRateSet({
                "coal vein": 9
            }),
            results=MaterialRateSet({
                "coal": 12
            }),
            time=1,
            factory="miner",
            name="mine coal",
        ))
        
        # smash coal to graphite
        recipes.add_recipe(ProductionRecipe(
            ingredients=MaterialRateSet({
                "coal": 3
            }),
            results=MaterialRateSet({
                "graphite": 1
            }),
            time=1/3,
            factory="graphite press",
            name="press graphite",
        ))
        
        graph = build_recipe_result_graph(MaterialRate("graphite", 5), recipes)

        for node in graph.nodes:
            print(node)
            if isinstance(node, ProductionNode):
                print(node.recipe.to_str())

        for edge in graph.edges:
            print(edge)

        render(graph, Path("./tests/test_data/graph/test_graph.png"))
