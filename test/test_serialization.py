import unittest
import json

from production_planning.objects.material_rate_set import MaterialRateSet
from production_planning.objects.recipes_collection import RecipesCollection, ProductionRecipe


class TestSerialization(unittest.TestCase):

    def test_recipe_collection_serialization(self):
        recipes = RecipesCollection()
        recipes.add_basic_material("coal")
        recipes.add_recipe(ProductionRecipe(
            ingredients=MaterialRateSet({
                "coal": 3
            }),
            results=MaterialRateSet({
                "graphite": 1
            }),
            time=1/3,
            factory="graphite press",
            name="press graphite",
        ))
        
        recipes_dict = recipes.to_dict()
        with open("./tests/test_data/recipes/parsed_recipes.json", "w+") as f:
            json.dump(recipes_dict, f)

        jstr = json.dumps(recipes_dict)
        loaded = RecipesCollection.from_json(json.loads(jstr))
        
        self.assertEqual(recipes, loaded)
    