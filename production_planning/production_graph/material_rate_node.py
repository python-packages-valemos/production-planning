from dataclasses import dataclass, field

from graph_toolkit.graph import GraphNode
from ..objects.material_rate import MaterialRate


@dataclass(unsafe_hash=True, repr=False)
class MaterialRateNode(GraphNode):
    rate: MaterialRate = field(default_factory=MaterialRate, hash=True)

    def calculate_id(self) -> str:
        return str(hash(self.rate))
    
    def to_str_debug(self):
        return f"M{self.id}({self.rate.material} x {self.rate.amount:.3f})"

    def to_str(self):
        return f"{self.rate.material} x {self.rate.amount:.3f} item/s"

    def merged(self, other) -> list:
        assert isinstance(other, self.__class__)
        assert self.rate.material == other.rate.material
        
        rate = MaterialRate(self.rate.material, self.rate.amount + other.rate.amount)
        return MaterialRateNode(rate=rate)
    