from typing import Optional
import copy

from ...objects import ProductionRecipe, MaterialRate
from ..production_graph import ProductionGraph, ProductionNode, MaterialRateNode, RequirementNode


class ProductionGraphBuilder:
    GraphClass = ProductionGraph

    def __init__(self, graph: Optional[GraphClass] = None) -> None:
        self._graph = self.GraphClass() if graph is None else graph

    @property
    def graph(self):
        return self._graph

    @graph.setter
    def graph(self, value):
        assert isinstance(value, self.GraphClass)
        self._graph = value

    def add_recipe_node(self, recipe, scaling):
        node: ProductionNode = self._graph.create_production_node(recipe=recipe)
        node.scaling = scaling
        return node

    def add_recipe_by_output(self, 
                             recipe: ProductionRecipe, 
                             output_rate: MaterialRate) -> ProductionNode:
        scaling = self._get_recipe_scaling(recipe, output_rate)
        return self.add_recipe_node(recipe, scaling)

    def add_material_rate_node(self, material_rate: MaterialRate) -> MaterialRateNode:
        return self._graph.create_material_rate_node(rate=copy.copy(material_rate))

    def add_requirement_node(self, material_rate: MaterialRate) -> MaterialRateNode:
        return self._graph.create_requirement_node(rate=copy.copy(material_rate))

    def build_edge(self, node_from, node_to, rate):
        self._graph.create_edge(node_from, node_to, rate=copy.copy(rate))

    def remove_edge(self, node_from, node_to):
        self._graph.remove_edges(node_from, node_to)

    def replace_node(self, target: GraphClass.NodeType, new_node: GraphClass.NodeType):
        self._graph.replace_nodes([target], new_node)

    def replace_nodes(self, targets: list[GraphClass.NodeType], new_node: GraphClass.NodeType):
        self._graph.replace_nodes(targets, new_node)

    def get_requirements(self) -> list[RequirementNode]:
        return list(self._graph.requirement_nodes)

    def build_node_requirements(self, node: GraphClass.NodeType) -> None:
        if not isinstance(node, ProductionNode):
            raise ValueError(f"cannot build requirements for {str(type(node))}")
        for ingredient_rate in node.get_requirements():
            ingredient_node = self.add_requirement_node(ingredient_rate)
            self.build_edge(ingredient_node, node, ingredient_rate)

    def build_node_results(self, node: GraphClass.NodeType) -> list[MaterialRateNode]:
        if not isinstance(node, ProductionNode):
            raise ValueError(f"cannot build result nodes for {str(type(node))}")
        result_nodes = []
        for result_rate in node.get_results():
            result_node = self.add_material_rate_node(result_rate)
            self.build_edge(node, result_node, result_rate)
            result_nodes.append(result_node)
        return result_nodes

    def _get_recipe_scaling(self, recipe: ProductionRecipe, output_rate: MaterialRate):
        if recipe.time == 0:
            return recipe.ingredients, 1
        
        yield_per_recipe = recipe.results.rates[output_rate.material]
        return recipe.time * output_rate.amount / yield_per_recipe
