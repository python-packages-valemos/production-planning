import itertools
import copy

from ...objects import ProductionRecipe, MaterialRateSet, MaterialRate
from ..production_graph import ProductionGraph, ProductionNode, ProductionEdge, MaterialRateNode, RequirementNode
from ..production_recipe_builder import ProductionRecipeBuilder
from .graph_builder import ProductionGraphBuilder

from .render_graph import get_dot
from debug_graph import send_dot_message

import logging

log = logging.getLogger(__name__)


class RateNodeStock:
    def __init__(self) -> None:
        self._total_results = MaterialRateSet()
        self._material_nodes: dict[str, list[MaterialRateNode]] = {}
    
    @property
    def material_nodes(self) -> dict[str, list[MaterialRateNode]]:
        return self._material_nodes
    
    def add_node(self, rate_node: MaterialRateNode):
        if rate_node.rate.amount <= 0:
            return
        self._total_results.add(rate_node.rate)
        if rate_node.rate.material not in self._material_nodes:
            self._material_nodes[rate_node.rate.material] = []
        self._material_nodes[rate_node.rate.material].append(rate_node)

    def remove_node(self, rate_node: MaterialRateNode):
        material = rate_node.rate.material
        if material not in self._material_nodes:
            return
        result_nodes = self._material_nodes[material]
        if rate_node in result_nodes:
            result_nodes.remove(rate_node)
        if len(result_nodes) == 0:
            del self._material_nodes[material]
        self._total_results.add(rate_node.rate * -1)
    
    def is_material_in_stock(self, material: str):
        return material in self._material_nodes


def add_missing_requirements(graph_builder: ProductionGraphBuilder):
    for production in graph_builder.graph.production_nodes:
        input_edges = production.inputs
        inputs = MaterialRateSet.from_rates(map(lambda e: e.rate, input_edges))
        input_diff = production.get_requirements().concat(inputs*-1)
        for rate in filter(_is_not_zero_material, input_diff):
            requirement = graph_builder.add_requirement_node(rate)
            graph_builder.build_edge(requirement, production, rate)
        
        output_edges = production.outputs
        outputs = MaterialRateSet.from_rates(map(lambda e: e.rate, output_edges))
        output_diff = production.get_results().concat(outputs*-1)
        for rate in filter(_is_not_zero_material, output_diff):
            material_node = graph_builder.add_material_rate_node(rate)
            graph_builder.build_edge(production, material_node, rate)


def _is_not_zero_material(_rate: MaterialRate):
    return not _is_near_zero(_rate.amount)


def _is_near_zero(value: float):
    return abs(value) < 1e-12


def satisfy_graph_requirements(graph_builder: ProductionGraphBuilder, 
                       recipe_builder: ProductionRecipeBuilder) -> ProductionGraphBuilder:
    stock = RateNodeStock()
    for material_rate_node in graph_builder.graph.material_rate_nodes:
        stock.add_node(material_rate_node)

    # TODO: on each production node change, add missing requirements
    has_any_changes = True
    while has_any_changes:
        has_any_changes = False
        for requirement in graph_builder.get_requirements():
            if not requirement.is_production_scaling:
                if requirement.recyclable:
                    continue
                if stock.is_material_in_stock(requirement.rate.material):
                    requirement.recyclable = True
                    continue
            results_nodes = try_satisfy_requirement(
                requirement, graph_builder, recipe_builder)
            for result in results_nodes:
                stock.add_node(result)
            has_any_changes = has_any_changes or (len(results_nodes) > 0)
            send_dot_message(get_dot(graph_builder.graph))
    
    for requirement in graph_builder.get_requirements():
        requirement.recyclable = False
    
    graph = graph_builder.graph
    graph = merge_compatible_nodes(graph)
    graph = balance_material_nodes(graph)
    graph = simplify_material_rate_connections(graph)
    graph_builder.graph = graph


def try_satisfy_requirement(
    requirement: RequirementNode,
    graph_builder: ProductionGraphBuilder, 
    recipe_builder: ProductionRecipeBuilder):
        
    recipe = recipe_builder.get_material_recipe(requirement.rate.material)
    if recipe is None:
        rate_node = graph_builder.add_material_rate_node(requirement.rate * -1)
        graph_builder.replace_node(requirement, rate_node)
        return []
    
    log.info(f"building recipe '{recipe.name}'")
    if requirement.is_production_scaling:
        production_node = graph_builder.add_recipe_node(recipe, requirement.rate.amount)
    else:
        production_node = graph_builder.add_recipe_by_output(recipe, requirement.rate)
        
    # connect production results to requirement
    result_nodes = graph_builder.build_node_results(production_node)
    main_result = next(filter(
        lambda n: n.rate.material == requirement.rate.material, 
        result_nodes))
    
    combined_result = graph_builder.add_material_rate_node(MaterialRate(requirement.rate.material))
    graph_builder.replace_nodes([requirement, main_result], combined_result)

    graph_builder.build_node_requirements(production_node)
    return result_nodes


def merge_node_pair(first, second):
    try:
        if isinstance(first, RequirementNode) and isinstance(second, MaterialRateNode):
            assert not first.is_production_scaling
            assert first.rate.material == second.rate.material
            return merge_requirement_material_nodes(first, second)
        elif isinstance(first, MaterialRateNode) and isinstance(second, RequirementNode):
            return merge_node_pair(second, first)
        return first.merged(second)
    except AssertionError:
        pass
    return None


def merge_requirement_material_nodes(first: RequirementNode, second: MaterialRateNode):
    if first.rate.amount > second.rate.amount:
        return RequirementNode(
            rate=first.rate + (second.rate * -1), 
            is_production_scaling=False)
    return MaterialRateNode(rate=second.rate + (first.rate * -1))


def merge_compatible_nodes(graph: ProductionGraph):
    from ..operations import merge_equal_nodes
    
    graph = merge_equal_nodes(graph, merge_node_pair)
    
    # convert non leaf requirements to leafs
    for requirement in filter(lambda n: graph.is_leaf_node(n), graph.requirement_nodes):
        empty_material = graph.create_material_rate_node(
            rate=MaterialRate(requirement.rate.material, 0))
        new_requirement = graph.create_requirement_node(
            rate=requirement.rate, is_production_scaling=False)
        graph.replace_nodes([requirement], empty_material)
        graph.create_edge(new_requirement, empty_material, rate=requirement.rate)
    return graph


def balance_material_nodes(graph: ProductionGraph):
    graph = copy.deepcopy(graph)
    
    for rate_node in graph.material_rate_nodes:
        total_flow = MaterialRate(rate_node.rate.material)
        for edge in graph.edges:
            if edge.node_from is edge.node_to:
                graph.edges.remove(edge)
                continue
            if edge.node_to is rate_node:
                total_flow = total_flow + edge.rate
            if edge.node_from is rate_node:
                total_flow = total_flow + (edge.rate * -1)
        rate_node.rate = total_flow
        
    return graph


def simplify_material_rate_connections(graph: ProductionGraph):
    graph = copy.deepcopy(graph)
    nodes = list(filter(lambda mr: _is_near_zero(mr.rate.amount), graph.material_rate_nodes))
    
    for node in nodes:
        if len(node.inputs) == 0 or len(node.outputs) == 0 or \
            (len(node.inputs) > 1 and len(node.outputs) > 1):
            continue

        if len(node.inputs) == 1:
            rates = list(map(lambda e: e.rate, node.outputs))
            from_nodes = itertools.repeat(node.inputs[0].node_from)
            to_nodes = map(lambda e: e.node_to, node.outputs)
        else: # len(outputs) == 1 is guaranteed
            rates = list(map(lambda e: e.rate, node.inputs))
            from_nodes = map(lambda e: e.node_from, node.inputs)
            to_nodes = itertools.repeat(node.outputs[0].node_to)
    
        for node_from, node_to, rate in zip(from_nodes, to_nodes, rates):
            graph.create_edge(node_from, node_to, rate=rate)
        graph.remove_node(node)
    
    return graph


def build_recipe_result_graph(material_rate: MaterialRate, 
                              recipe_builder: ProductionRecipeBuilder):
    graph_builder = ProductionGraphBuilder()
    graph_builder.graph.create_requirement_node(rate=material_rate, is_production_scaling=False)
    satisfy_graph_requirements(graph_builder, recipe_builder)
    return graph_builder.graph


def build_recipe_amount_graph(recipe_rate: MaterialRate, 
                              recipe_builder: ProductionRecipeBuilder):  
    graph_builder = ProductionGraphBuilder()
    graph_builder.graph.create_requirement_node(rate=recipe_rate, is_production_scaling=True)
    satisfy_graph_requirements(graph_builder, recipe_builder)
    return graph_builder.graph
