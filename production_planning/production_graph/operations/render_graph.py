import os
from pathlib import Path
from graph_toolkit.graph import Graph


def convert_to_graphviz(production_graph: Graph):
    from graphviz import Digraph
    graph = Digraph()
    
    for node in production_graph.nodes:
        graph.node(str(node.id), str(node), URL=str(node.id))
    
    for e in production_graph.edges:
        graph.edge(str(e.node_from.id), str(e.node_to.id), str(e))
    
    return graph
    

def render(production_graph: Graph, path: os.PathLike):
    if isinstance(path, str):
        path = Path(path)

    graph = convert_to_graphviz(production_graph)
    graph.render(path.with_suffix("").name,
                path.parent,
                format=''.join(filter(lambda c: c != '.', path.suffix)))

def get_dot(production_graph: Graph):
    graph = convert_to_graphviz(production_graph)
    return (''.join(graph)).encode("utf-8")