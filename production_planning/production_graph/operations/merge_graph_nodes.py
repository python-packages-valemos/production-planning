import copy

from ...objects.material_rate_set import MaterialRate
from ..production_graph import ProductionGraph, ProductionEdge

from graph_toolkit.graph import GraphNode

import logging

log = logging.getLogger(__name__)


def _default_merge(first: GraphNode, second: GraphNode):
    try:
        return first.merged(second)
    except Exception:
        return None


def merge_equal_nodes(production_graph: ProductionGraph, 
                      merge_nodes=_default_merge):
    merged_graph = copy.deepcopy(production_graph)
    
    ungrouped_nodes = list(merged_graph.nodes)
    while len(ungrouped_nodes) > 0:
        merged_node = ungrouped_nodes.pop(0)
        node_group = [merged_node]
        for other_node in ungrouped_nodes:
            new_node = merge_nodes(merged_node, other_node)
            if new_node is not None:
                merged_node = new_node
                node_group.append(other_node)
        
        if len(node_group) > 1:
            merged_graph.replace_nodes(node_group, merged_node)
            node_group.pop(0)
            for node in node_group:
                ungrouped_nodes.remove(node)

    _merge_similar_edges(merged_graph)
    return merged_graph


def _merge_similar_edges(result: ProductionGraph):
    edge_groups: dict[(str, object, object), list[ProductionEdge]] = {}
    
    # group by material type
    for edge in result.edges:
        key = edge.rate.material, edge.node_from.id, edge.node_to.id
        same_node_list = edge_groups.get(key, [])
        same_node_list.append(edge)
        edge_groups[key] = same_node_list
    
    for (material_type, _, _), edge_group in edge_groups.items():
        total_rate = sum(map(lambda e: e.rate.amount, edge_group))
        material_rate = MaterialRate(material_type, total_rate)
        result.create_edge(edge_group[0].node_from, edge_group[0].node_to, rate=material_rate)
        for edge in edge_group:
            result.edges.remove(edge)
