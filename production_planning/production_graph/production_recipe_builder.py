from typing import Optional
from ..objects.recipes_collection import RecipesCollection, ProductionRecipe
from ..objects.material_rate import MaterialRate

import logging

log = logging.getLogger(__name__)


def _material_recipes_ambiguous_message(material: str, recipes: list[ProductionRecipe]):
    recipes_names = '\n'.join(map(lambda r: r.name, recipes))
    return (f"There are more than 1 recipe for {material}\n"
            "Recipe choice is ambiguous. "
            f"Select preferred recipe from:\n{recipes_names}")


class RecipeAmbiguousError(RuntimeError):
    def __init__(self, material: str, recipe_names: list[ProductionRecipe]) -> None:
        super().__init__(_material_recipes_ambiguous_message(material, recipe_names))
        self._material = material
        self._recipe_names = recipe_names


class ProductionRecipeBuilder:
    def __init__(self, recipes_collection: RecipesCollection, raise_on_ambiguity=False) -> None:
        self._recipes_collection = recipes_collection
        self._raise_on_ambiguity = raise_on_ambiguity
    
    @property
    def recipes_collection(self) -> RecipesCollection:
        return self._recipes_collection
    
    def get_recipe(self, recipe_name: str):
        return self._recipes_collection.get_recipe(recipe_name)
    
    def get_material_recipe(self, material: str) -> Optional[ProductionRecipe]:
        recipe = self.get_recipe_or_raise(material)
        return recipe
    
    def get_recipe_or_raise(self, material: str) -> Optional[ProductionRecipe]:
        recipes = self._recipes_collection.get_material_recipes(material)
        if len(recipes) == 0:
            return None
        if len(recipes) == 1:
            return recipes[0]
        else:
            if self._raise_on_ambiguity:
                raise RecipeAmbiguousError(material, recipes)
            else:
                log.warning(_material_recipes_ambiguous_message(material, recipes))
