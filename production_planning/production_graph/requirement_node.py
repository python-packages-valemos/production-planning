from dataclasses import dataclass, field

from graph_toolkit.graph import GraphNode
from ..objects.material_rate import MaterialRate


@dataclass(unsafe_hash=True, repr=False)
class RequirementNode(GraphNode):
    rate: MaterialRate = field(default_factory=MaterialRate, hash=True)
    recyclable: bool = False
    is_production_scaling: bool = False

    def calculate_id(self) -> str:
        return str(hash(self.rate))
    
    def to_str_debug(self):
        return f"R{self.id}({self.rate.material} x {self.rate.amount})"

    def to_str(self):
        return "Requires" \
            + (" ↻" if self.recyclable else "") \
            + (" 🏭" if self.is_production_scaling else "") \
            + f"\n{self.rate.material} x {self.rate.amount:.3f} item/s"

    def merged(self, other) -> list:
        assert isinstance(other, self.__class__)
        assert self.rate.material == other.rate.material
        assert self.is_production_scaling == other.is_production_scaling
        
        return RequirementNode(
            rate=MaterialRate(self.rate.material, self.rate.amount + other.rate.amount),
            is_production_scaling=self.is_production_scaling)
