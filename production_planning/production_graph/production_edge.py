from ..objects.material_rate import MaterialRate
from graph_toolkit.graph import GraphEdge


class ProductionEdge(GraphEdge):
    
    def __init__(self, 
                 node_from, node_to, 
                 rate: MaterialRate = None) -> None:
        super().__init__(node_from, node_to)
        self._rate = rate if rate is not None else MaterialRate("")
    
    @property
    def rate(self):
        return self._rate
    
    @rate.setter
    def rate(self, value):
        self._rate = value
   
    def to_str(self):
        return f"{self.rate.material} x {self.rate.amount:.3f} items/s"

    def to_str_debug(self):
        return (f"{self.node_from.id} -"
            f"({self.rate.material} x {self.rate.amount:.1f})-> "
            f"{self.node_to.id}")
