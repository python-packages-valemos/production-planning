from typing import Iterator, Optional, Union

from graph_toolkit.graph import Graph, GraphNode
from .material_rate_node import MaterialRateNode
from .production_node import ProductionNode
from .production_edge import ProductionEdge
from .requirement_node import RequirementNode


class ProductionGraph(Graph):
    EdgeClass = ProductionEdge
    NodeType = Union['ProductionNode', 'MaterialRateNode', 'RequirementNode']
    
    nodes: list[NodeType]
    edges: list[EdgeClass]
    
    def get_node_by_id(self, node_id) -> Optional[NodeType]:
        for node in self.nodes:
            if node.id == node_id:
                return node
        return None

    @property
    def production_nodes(self) -> Iterator[ProductionNode]:
        return filter(lambda n: isinstance(n, ProductionNode), self.nodes)
        
    @property
    def material_rate_nodes(self) -> Iterator[MaterialRateNode]:
        return filter(lambda n: isinstance(n, MaterialRateNode), self.nodes)
    
    @property
    def requirement_nodes(self) -> Iterator[RequirementNode]:
        return filter(lambda n: isinstance(n, RequirementNode), self.nodes)
        
    def create_production_node(self, recipe, scaling=1):
        return self.create_node(ProductionNode, recipe=recipe, scaling=scaling)

    def create_material_rate_node(self, rate):
        return self.create_node(MaterialRateNode, rate=rate)

    def create_requirement_node(self, *args, **kwargs):
        return self.create_node(RequirementNode, *args, **kwargs)
