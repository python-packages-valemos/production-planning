from dataclasses import dataclass, field

from ..objects.production_recipe import ProductionRecipe
from graph_toolkit.graph import GraphNode


@dataclass(repr=False)
class ProductionNode(GraphNode):
    recipe: ProductionRecipe = field(default_factory=ProductionRecipe)
    scaling: float = 1
    
    def get_results(self):
        return self.recipe.results * (self.scaling / self.recipe.time)
    
    def get_requirements(self):
        return self.recipe.ingredients * (self.scaling / self.recipe.time)
    
    def calculate_id(self) -> str:
        return str(hash(self.recipe.name))
    
    def to_str_debug(self):
        return f"P{self.id}({self.recipe.name}) x {self.scaling:.1f}"

    def to_str(self):
        return f"{self.recipe.name} x {self.scaling:.1f} ({self.recipe.time:.3f}s)\n"\
            f"at {self.recipe.factory}"

    def merged(self, other) -> list:
        assert isinstance(other, self.__class__)
        assert self.recipe == other.recipe
        return ProductionNode(recipe=self.recipe, 
                              scaling=self.scaling + other.scaling)
    