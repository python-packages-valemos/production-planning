import copy
from typing import Optional
import re

from .recipes_collection import RecipesCollection

import logging

log = logging.getLogger(__name__)


class RecipePreferenceBuilder:
    
    def __init__(self, recipes_collection: RecipesCollection) -> None:
        self._recipes_collection = recipes_collection
        self._trimmed_collection = recipes_collection.__class__()

    def set_preference_dict(self, preferences: dict[str, str]):
        for material_name, recipe_name in preferences.items():
            self.set_preferred_recipe(material_name, recipe_name)
    
    def set_preferred_recipe(self, material: str, preferred_recipe_name: Optional[str]):
        """
        If preferred recipe name was already selected from its group, 
        new value will override it
        """
        
        self._trimmed_collection.remove_material_recipes(material)
        
        if preferred_recipe_name is None:
            self._trimmed_collection.add_basic_material(material)
            return
        
        recipes = self._recipes_collection.get_material_recipes(material)
        recipe_filter = filter(lambda r: r.name == preferred_recipe_name, recipes)
        try:
            recipe = next(recipe_filter)
            self._trimmed_collection.add_material_recipe(material, recipe)
        except Exception:
            return
        
    def get_preferred(self):
        for material in self._recipes_collection._recipe_by_material.keys():
            if not self._trimmed_collection.is_material_known(material):
                for recipe in self._recipes_collection.get_material_recipes(material):
                    self._trimmed_collection.add_material_recipe(material, recipe)
        
        return self._trimmed_collection
