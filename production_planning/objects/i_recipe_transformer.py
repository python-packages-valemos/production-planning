from abc import abstractmethod
from .production_recipe import ProductionRecipe


class IRecipeTransformer:
    
    @abstractmethod
    def transform(self, recipe: ProductionRecipe) -> ProductionRecipe:
        pass
    