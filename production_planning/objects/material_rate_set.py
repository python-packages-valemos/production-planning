from copy import deepcopy
from dataclasses import dataclass, field
from dataclasses_json import DataClassJsonMixin
from typing import Iterator, Iterable

from ..objects.material_rate import MaterialRate


@dataclass
class MaterialRateSet(DataClassJsonMixin):
    rates: dict[str, float] = field(default_factory=dict)
    
    def __iter__(self) -> Iterator[MaterialRate]:
        for material, rate in self.rates.items():
            yield MaterialRate(material, rate)
    
    def __getitem__(self, material_type):
        return MaterialRate(material_type, self.rates[material_type])
    
    def __contains__(self, item):
        return item in self.rates
    
    def __mul__(self, multiplier):
        assert isinstance(multiplier, float) or isinstance(multiplier, int)

        new_collection = MaterialRateSet()
        for item in self:
            new_collection.add(item * multiplier)

        return new_collection

    def __truediv__(self, divider):
        assert isinstance(divider, float) or isinstance(divider, int)
        return self * (1 / divider)

    @staticmethod
    def from_rates(rates: Iterable[MaterialRate]):
        rate_set = MaterialRateSet()
        for rate in rates:
            rate_set.add(rate)
        return rate_set
    
    def add(self, rate: MaterialRate):
        if rate.material in self.rates:
            self.rates[rate.material] += rate.amount
        else:
            self.rates[rate.material] = rate.amount
    
    def concat(self, other):
        assert isinstance(other, self.__class__)

        new_collection = deepcopy(self)

        for material in other:
            new_collection.add(material)

        return new_collection

    def to_dict(self):
        return [MaterialRate(material, rate).to_dict() for material, rate in self.rates.items()]

    @classmethod
    def from_json(cls, json_object: dict, *args, **kwargs):
        return cls.from_rates((MaterialRate.from_json(o, *args, **kwargs) for o in json_object))
