from dataclasses import dataclass, field
from dataclasses_json import DataClassJsonMixin

from .material_rate_set import MaterialRateSet, MaterialRate


@dataclass
class ProductionRecipe(DataClassJsonMixin):
    name: str = ""
    factory: str = None
    time: float = 0
    ingredients: MaterialRateSet = field(default_factory=MaterialRateSet)
    results: MaterialRateSet = field(default_factory=MaterialRateSet)

    def to_str(self):
        def _rate_to_str(rate: MaterialRate):
            return f"{rate.material} x {rate.amount:.3f}"
            
        ingredients = '\n'.join(map(_rate_to_str, self.ingredients))
        results = '\n'.join(map(_rate_to_str, self.results))
        return f"{self.name}:\n"\
            f"{ingredients}\n"\
            f"|\n|  {self.time:.3f} sec on {self.factory}\n\\/\n"\
            f"{results}"

    @classmethod
    def from_dict_impute_values(cls, json_object, *args, **kwargs):
        obj = ProductionRecipe.from_dict(json_object, *args, **kwargs)
        if obj.name == "":
            obj.name = obj.factory
        return obj
