from dataclasses import dataclass, field
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass(unsafe_hash=True)
class MaterialRate:
    material: str = field(hash=True, default="")
    amount: float = field(default=0, hash=True)

    def __mul__(self, multiplier):
        assert isinstance(multiplier, float) or isinstance(multiplier, int)
        return MaterialRate(self.material, self.amount * multiplier)

    def __add__(self, other):
        assert issubclass(other.__class__, self.__class__)
        if other.material != self.material: raise ValueError("cannot add different items")
        return MaterialRate(self.material, self.amount + other.amount)

    def __eq__(self, other):
        assert issubclass(other.__class__, self.__class__)
        return other.material == self.material and other.amount == self.amount

    @property
    def id(self):
        return hash(self)