from typing import Optional, Union

from .material_rate_set import MaterialRateSet
from .production_recipe import ProductionRecipe
from .i_recipe_transformer import IRecipeTransformer

import logging

log = logging.getLogger(__name__)


class RecipesCollection:

    def __init__(self) -> None:
        self._ready_materials: set[str] = set()
        self._recipes: dict[str, ProductionRecipe] = {}
        self._recipe_by_material: dict[str, list[str]] = {}

    def __getitem__(self, key):
        return self.get_material_recipes(key)

    def __eq__(self, other):
        return self._recipes == other._recipes and \
               self._ready_materials == other._basic_materials

    @property
    def recipes_iter(self):
        return self._recipes.values()

    @property
    def recipes(self):
        return list(self.recipes_iter)

    @property
    def ready_materials(self):
        return list(self._ready_materials)

    @property
    def produced_materials(self):
        return list(self._recipe_by_material.keys())

    def is_recipe_exists(self, material: str):
        return material in self._recipe_by_material

    def is_basic_material(self, material: str):
        return material in self._ready_materials

    def is_material_known(self, material: str):
        return self.is_basic_material(material) or self.is_recipe_exists(material)

    def add_basic_material(self, material: str):
        self._ready_materials.add(material)

    def add_recipe(self, recipe: ProductionRecipe):
        if recipe.name in self._recipes:
            self.remove_recipe(recipe)
        
        for result in recipe.results:
            self.add_material_recipe(result.material, recipe)

    def get_recipe(self, recipe_name: str) -> ProductionRecipe:
        if recipe_name not in self._recipes:
            raise ValueError(f'no recipe with name "{recipe_name}"')
        return self._recipes[recipe_name]

    def get_factory_recipes(self, factory: str) -> list[ProductionRecipe]:
        return list(filter(lambda r: r.factory == factory, self._recipes.values()))
    
    def get_material_recipes(self, material: str) -> list[ProductionRecipe]:
        if material in self._ready_materials:
            return []
        
        return self._recipe_by_material.get(material, [])

    def remove_recipe(self, recipe: ProductionRecipe):
        for material_rate in recipe.results:
            if material_rate.material in self._recipe_by_material:
                recipes = self._recipe_by_material[material_rate.material]
                if recipe.name in recipes:
                    recipes.remove(recipe.name)
            
        del self._recipes[recipe.name]

    def remove_recipe_by_name(self, name):
        recipe = self._recipes.get(name, None)
        if recipe is not None:
            self.remove_recipe(recipe)

    def remove_material_recipes(self, material: str):
        for recipe_name in self._recipe_by_material.get(material, []):
            self.remove_recipe_by_name(recipe_name)
    
    def add_material_recipe(self, material: str, recipe: ProductionRecipe):
        if recipe.name not in self._recipes:
            self._recipes[recipe.name] = recipe
        
        if material not in self._recipe_by_material:
            recipe_list = []
            self._recipe_by_material[material] = recipe_list
        else:
            recipe_list = self._recipe_by_material[material]
        
        if len(recipe_list) == 0:
            if material in self._ready_materials:
                self._ready_materials.remove(material)
        recipe_list.append(recipe)

    def to_dict(self) -> dict:
        return {
            "basic": list(self._ready_materials),
            "composite": [recipe.to_dict() for recipe in self._recipes.values()]
        }

    @classmethod
    def from_dict(cls, json_object: dict):
        collection = cls()
        for basic in json_object["basic"]:
            collection.add_basic_material(basic)

        for recipe_json in json_object["composite"]:
            recipe = ProductionRecipe.from_dict_impute_values(recipe_json)
            collection.add_recipe(recipe)

        return collection
